# Assignment 6 - Pokemon Trainer Hub

The application can be accessed [here](https://pokemon-trainer-hub-wermel.herokuapp.com/).

The trainer API can be accessed [here](https://pokemon-api-mw.herokuapp.com/trainers)

Pokemon Trainer Hub is a simple Angular application where user can view pokemon from all generations, catch them, as well as remove them.

Only one pokemon of each species is allowed.

The user may log in using her username. If the name should be used before, the list of previously caught pokemon will be fetched.

Information about the user and the pokemon catalogue is being stored in the local storage, as long as used does not log out or the storage is not cleaned up.

## Development

Application uses 3rd party package `ngx-pagination` in order to simplify the implementation of paginator on the catalogue page.

Run `npm install` to install all dependencies before executing application locally.

Run the application using `npm start`.