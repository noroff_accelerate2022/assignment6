// data structure representing a pokemon
export interface Pokemon {
    id: number,
    name: string,
    avatar: string,
}

// *** structures corresponding to API responses ***

export interface PokemonResponse {
    count: number,
    next: string,
    previous: string,
    results: ServerPokemon[]
}

export interface ServerPokemon {
    "name": string,
    "url": string,
}

// used when retrieving by name
export interface SingleServerPokemon {
    "id": number,
}