import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/internal/operators/filter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'assignment6-pokemon-trainer';
  routeUrl: string = '';

  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      this.routeUrl = this.router.url;
    })
  }
}
