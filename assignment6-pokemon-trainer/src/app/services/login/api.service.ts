import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map, Observable, of, switchMap } from 'rxjs';
import { Injectable } from '@angular/core';
import { Trainer } from '../../models/trainer.model';
import { Pokemon } from 'src/app/models/pokemon.model';

const { trainerURL, apiKey } = environment

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  //dependency injection
  constructor( private readonly http: HttpClient) { }

  public login(username : string): Observable<Trainer> {
    return this.checkUsername(username)
      .pipe(
        switchMap((trainer: Trainer | undefined) => {
          if (trainer === undefined) { //trainer does not exist
            return this.createTrainer(username);
          }
          return of(trainer)
        })
      )
  }

  //check if a user exists
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${trainerURL}?username=${username}`)
      .pipe (
        //RxJS Operators
        map((response: Trainer[]) => response.pop())
      )
  }

  //create a trainer
  private createTrainer(username : string): Observable<Trainer>{
    const trainer = {
      username, 
      pokemon: []
    }
    
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    })
    
    return this.http.post<Trainer>(trainerURL, trainer, {
      headers
    })
  }

  public updatePokemon(id: number, pokemon: string[]): Observable<Trainer>{
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    })

    const body = {
      pokemon: pokemon,
    }

    return this.http.patch<Trainer>(`${trainerURL}/${id}`, body, {
      headers
    })
  }
}
