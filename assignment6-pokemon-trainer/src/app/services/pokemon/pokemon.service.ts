import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Pokemon, PokemonResponse, SingleServerPokemon } from 'src/app/models/pokemon.model';
import { StorageUtil } from 'src/app/utils/storage.util';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  _pokemonList?: Pokemon[];

  get pokemonList(): Pokemon[] | undefined {
    return this._pokemonList;
  }

  set pokemonList(pokemonList: Pokemon[] | undefined) {
    StorageUtil.storageSave(StorageKeys.Pokemon, pokemonList)
    this._pokemonList = pokemonList;
  }

  fetchPokemon() {
    return this.http
    .get<PokemonResponse>(`${environment.pokemonURL}?limit=1154&offset=0`)
  }

  findPokemonByName(name: string) {
    return this.http
    .get<SingleServerPokemon>(`${environment.pokemonURL}/${name}`)
  }

  constructor(private http: HttpClient) {
    this._pokemonList = StorageUtil.storageRead(StorageKeys.Pokemon);
  }
}
