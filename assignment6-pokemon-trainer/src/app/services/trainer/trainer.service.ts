import { Trainer } from 'src/app/models/trainer.model';
import { Injectable } from '@angular/core';
import { StorageKeys } from '../../enums/storage-keys.enum';
import { StorageUtil } from '../../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer

  get trainer(): Trainer | undefined {
    return this._trainer
  }

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!)
    this._trainer = trainer
  }

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer)
  }
}
