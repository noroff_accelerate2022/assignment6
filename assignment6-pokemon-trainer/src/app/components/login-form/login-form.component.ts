import { TrainerService } from './../../services/trainer/trainer.service';
import { Trainer } from '../../models/trainer.model';
import { ApiService } from '../../services/login/api.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter()

  constructor(
    private readonly router: Router,
    private readonly loginService: ApiService,
    private readonly trainerService: TrainerService,  
  ){ } 


  public loginSubmit(loginForm: NgForm): void {

    const { username } = loginForm.value

    this.loginService.login(username)
      .subscribe({
        next: (trainer: Trainer) => {
          this.trainerService.trainer = trainer
          this.login.emit()
        },
        error: () => {
          console.error('Could not log in');
        }
      })
  }

  // handle form submit - log in (create user if doesn't exist, save to the local storage), 
  // and redirect to the profile page
}
