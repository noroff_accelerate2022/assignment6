import { Location } from '@angular/common';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  @Input() routeUrl!: string;

  constructor(private router: Router) { 
    this.router = router;
  }

  logout() {
    // clean up the local storage
    if (window.confirm('Are you sure you want to logout?')) {
      localStorage.clear()
      window.location.reload()
      
    // navigate to the landing page
    this.router.navigateByUrl('');
    }
  }

  ngOnInit(): void {
  }

}
