import { TrainerService } from 'src/app/services/trainer/trainer.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pokemon } from '../../models/pokemon.model';
import { ApiService } from 'src/app/services/login/api.service';
import { Trainer } from 'src/app/models/trainer.model';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css']
})
export class PokemonCardComponent implements OnInit {

  @Input() pokemon!: Pokemon;

  // determines whether what type of button should be displayed
  @Input() isRemovable: boolean = true;

  @Output() update: EventEmitter<void> = new EventEmitter();
  trainer?: Trainer;

  constructor(private trainerService: TrainerService, private apiService: ApiService) { 
      this.trainer = this.trainerService.trainer;
  }

  ngOnInit(): void {
  }

  isCaught(): boolean {
    if (this.trainer?.pokemon.includes(this.pokemon.name)) return true;
    return false;
  }

  click() {
    let newPokemonList: string[] = [];

    if (this.trainerService.trainer) {
    
      if (this.isRemovable) {
        if (window.confirm('Are you sure you want to remove this pokemon?')) {
          newPokemonList = this.trainerService.trainer?.pokemon.filter(p => p !== this.pokemon.name);
        }
      } else {
        newPokemonList = this.trainerService.trainer?.pokemon.concat([this.pokemon.name]);
      }

        // send the request only if there were updates to pokemon list
        if (newPokemonList.length > 0) {
        this.apiService.updatePokemon(this.trainerService.trainer.id, newPokemonList)
        .subscribe({
          next: (trainer: Trainer) => {
            this.trainerService.trainer = trainer;
            this.trainer = this.trainerService.trainer;
            this.update.emit();
          },
          error: () => {
            console.error("Could not update the pokemon list");
          }
        })
      }
    }
  }

}
