import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';
import { TrainerService } from 'src/app/services/trainer/trainer.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
export class CataloguePage implements OnInit {
  pokemonList?: Pokemon[];

  // starts at the 1st page, shows first 10 results
  p: number = 1;
  resultsPerPage = 10;

  constructor(private pokemonService: PokemonService) { 
    this.pokemonList = this.pokemonService.pokemonList;

  }

  ngOnInit(): void {
    // fetch, map and set pokemon if local storage is empty
    if (!this.pokemonList) {
      this.pokemonService.fetchPokemon()
      .subscribe(resp => {
          // map the response to Pokemon objects
          const pokemonList: Pokemon[] = resp.results.map((pokemon): Pokemon => {
              // retrieve pokemon's id from URL
              const pokemonId = pokemon.url.match(/(?<=\/)\d+(?=\/)/gm)?.pop();
              const imgUrl = `${environment.pokemonImgUrl}${pokemonId}.png`;
  
              return {
                  id: pokemonId ? parseInt(pokemonId) : 0,
                  name: pokemon.name,
                  avatar: imgUrl
              }
          });
          this.pokemonList = pokemonList;
          this.pokemonService.pokemonList = pokemonList;
      });
    }    
  }
}