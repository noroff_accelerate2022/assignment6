import { Component, OnInit } from '@angular/core';
import { Pokemon, SingleServerPokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';
import { TrainerService } from 'src/app/services/trainer/trainer.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
  trainer: Trainer | undefined;
  trainerPokemon: Pokemon[] = [];

  constructor(private trainerService: TrainerService, private pokemonService: PokemonService) { 
    this.trainer = this.trainerService.trainer;
  }

  // invoked on "REMOVE" button click
  handleTrainerUpdate() {
    this.trainer = this.trainerService.trainer;
    this.trainerPokemon = [];
    this.loadTrainerPokemon();
  }

  // map pokemon names stored in Trainer object to Pokemon objects with 
  // relevant ID and picture
  private loadTrainerPokemon() {
    this.trainer?.pokemon.forEach(pokStr => {
      this.pokemonService.findPokemonByName(pokStr)
      .subscribe({
        next: (pok: SingleServerPokemon) => {
          const pokemonObj: Pokemon = {
            id: pok.id,
            name: pokStr,
            avatar: `${environment.pokemonImgUrl}${pok.id}.png`
          }
          this.trainerPokemon?.push(pokemonObj);
        },
        error: () => {
          console.error("Could not load trainer pokemon");
        }
      })
    })
  }

  ngOnInit(): void {
    // map pokemon names to objects
    this.loadTrainerPokemon();
  }
}
