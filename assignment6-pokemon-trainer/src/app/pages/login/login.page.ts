import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { TrainerService } from 'src/app/services/trainer/trainer.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage {

  constructor(private readonly router: Router, private trainer: TrainerService) { }

  ngOnInit(): void {
    // redirect if user exists
    if (this.trainer.trainer) this.router.navigateByUrl("/catalogue");
  }

  handleLogin(): void {
    this.router.navigateByUrl("/catalogue")
  }
}
